# Example Repository Using Shared SAST Rules

This is an example of a repository using GitLab's IaC SAST Scanning (KICS) with
a shared ruleset from [another repository](https://gitlab.com/joshua.beard/security-policy-shared-example).
